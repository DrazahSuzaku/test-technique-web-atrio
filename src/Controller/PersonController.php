<?php

namespace App\Controller;

use App\Entity\Person;
use App\Form\Type\PersonType;
use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PersonController extends AbstractController
{
    private PersonRepository $personRepository;

    public function __construct(PersonRepository $personRepository){
        $this->personRepository = $personRepository;
    }

    #[Route('/', name: 'test-person')]
    public function index(Request $request): Response
    {
        $persons = $this->personRepository->findAll();
        $isList = $request->query->has('list');
        $birthdayError = false;

        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if($form->isSubmitted()) {
            if ($person->getAge() > 150) {
                $birthdayError = true;
            }
            if (!$birthdayError && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($person);
                $em->flush();
            }
        }

        return $this->render('person/index.html.twig', [
            'form' => $form->createView(),
            'isList' => $isList,
            'birthdayError' => $birthdayError,
            'persons' => $persons
        ]);
    }
}
